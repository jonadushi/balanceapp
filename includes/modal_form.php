<!-- Add Monew Flow Form -->
<!-- Modal -->
<div class="modal fade" id="new-money-flow" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Monew Flow</h4>
			</div>	       
			<form class="form-horizontal" data-toggle="validator" method="post">
				<div class="modal-body">
					<input class="hidden" name="type" id="type">
					<div class="form-group">
					<label for="title" class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10">
							<input type="text"  class="form-control" id="title" name="title" placeholder="Title" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="nr_max_personave" class="col-sm-2 control-label">Date</label>
						<div class="col-sm-10">
						<input type="text" name="date" id="date" class="dataora form-control" placeholder="YYYY-MM-DD" required>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
					<label for="kati" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10">
							<input type="text"   class="form-control" id="description" name="description" placeholder="Description" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="cmimi" class="col-sm-2 control-label">Amount</label>
						<div class="col-sm-10">
							<div class="input-group">
								<div class="input-group-addon">$</div>
						        <input type="number" class="form-control" id="amount" name="amount" placeholder="Amount" required>
						        <div class="input-group-addon">.00</div>
						        <div class="help-block with-errors"></div>
					        </div>
						</div>								    
					</div>		    
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button id="add-money-flow" class="btn btn-default">Add</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>			            
				</div>
			</form>       
		</div>	      
	</div>
</div>

<!-- Edit Money Flow Form -->
<!-- Modal -->
<div class="modal fade" id="edit-money-flow" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Money Flow</h4>
			</div>	       
			<form class="form-horizontal" data-toggle="validator" method="post">
				<div class="modal-body">
					<input class="hidden" name="id" id="id">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10">
							<input type="text"  class="form-control" id="title" name="title" placeholder="Title" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="nr_max_personave" class="col-sm-2 control-label">Date</label>
						<div class="col-sm-10">
						<input type="text" name="date" id="date" class="dataora form-control" placeholder="YYYY-MM-DD" required>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
					<label for="kati" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10">
							<input type="text"   class="form-control" id="description" name="description" placeholder="Description" required>
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="form-group">
						<label for="cmimi" class="col-sm-2 control-label">Amount</label>
						<div class="col-sm-10">
							<div class="input-group">
								<div class="input-group-addon">$</div>
						        <input type="number" class="form-control" id="amount" name="amount" placeholder="Amount" required>
						        <div class="input-group-addon">.00</div>
						        <div class="help-block with-errors"></div>
					        </div>
						</div>								    
					</div>		    
				</div>
				<div class="modal-footer">
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button id="update-money-flow" class="btn btn-default">Edit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>			            
				</div>
			</form>       
		</div>	      
	</div>
</div>

