-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2016 at 05:07 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `balance_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `money_flow`
--

CREATE TABLE `money_flow` (
  `id` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `description` varchar(50) NOT NULL,
  `amount` double NOT NULL,
  `type` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `money_flow`
--

INSERT INTO `money_flow` (`id`, `title`, `date`, `description`, `amount`, `type`) VALUES
(2, 'balance app', '0000-00-00', 'job application', 12, ''),
(3, 'balance app', '2016-07-25', 'job application', 12, ''),
(4, 'balance app', '2016-07-25', 'job application', 111, ''),
(5, 'balance app', '2016-07-25', 'grab tweets', 30, ''),
(6, 'balance111', '2016-07-25', 'job application', 123, ''),
(7, 'balance app', '2016-07-25', 'job application', 111, ''),
(8, 'balance appqqq', '2016-07-25', 'job application', 50, ''),
(18, 'balance app', '2016-01-25', 'job application', 124, 'expence'),
(19, 'balance app', '2016-01-25', 'job application', 900, 'expence'),
(21, 'balance app', '2016-01-25', 'job application', 1000, 'income');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `money_flow`
--
ALTER TABLE `money_flow`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `money_flow`
--
ALTER TABLE `money_flow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
