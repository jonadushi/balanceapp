<!DOCTYPE html>
<html lang="en">
<?php
    include("php/init_connection.php");

    $query = "SELECT * FROM money_flow WHERE type = 'income'";
    $incomes = mysql_query($query) or die(mysql_error());
    $query = "SELECT * FROM money_flow WHERE type = 'expence'";
    $expences = mysql_query($query) or die(mysql_error());
    
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Balance App</title>

    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <link href="css/creative.css" rel="stylesheet">

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Balance App</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#balance">Balance</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#expences">Expences</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#incomes">Incomes</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Keep Track of your balance</h1>
                <hr>
                <p>Register your incomes and expences to manage better your money!</p>
                <a href="#balance" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
            </div>
        </div>
    </header>

    <section id="balance">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <h2 class="section-heading">Your Monthly Balance</h2>
                    <hr class="primary"><br/><br/>
                <form class="form-inline" method="post" >
                  <div class="form-group">
                    <select name="month" id="month" class="form-control">
                      <option value="1">January</option>
                      <option value="2">February</option>
                      <option value="3">March</option>
                      <option value="4">April</option>
                      <option value="5">May</option>
                      <option value="6">June</option>
                      <option value="7">July</option>
                      <option value="8">August</option>
                      <option value="9">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="13">Decemder</option>
                    </select>
                  </div>
                </form>
                <br><br>
                <canvas id="myChart" width="300px" height="300px"></canvas>
                <br>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-primary" id="expences">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Your Expences</h2>
                    <a href = "#" id="new-expence" data-toggle="modal" data-target="#new-money-flow" style="color: white;">New Expence</a>
                    <hr class="light">
                    <table class="table" style="color: white;" id="table-expences">
                        <thead>
                            <th>#</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Desciption</th>
                            <th>Amount</th>
                            <th></th>
                        </thead>
                        <tbody >
                        <?php
                            while ($row = mysql_fetch_assoc($expences)) {  ?>
                            <tr data-id="<?php echo $row["id"]; ?>">
                                <td><?php echo $row["id"]; ?></td>
                                <td><?php echo $row["title"]; ?></td>
                                <td><?php echo $row["date"]; ?></td>
                                <td><?php echo $row["description"]; ?></td>
                                <td><?php echo $row["amount"]; ?></td>
                                <td class="text-right">
                                    <button class="edit-money-flow btn btn-default btn-xs" data-toggle="modal" data-target="#edit-money-flow">Edit</button>
                                    <button class="btn btn-default btn-xs del">Delete</button>
                                </td>
                            </tr>
                            <?php 
                            } 
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <section id="incomes" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Your Incomes</h2> 
                    <a href = "#" id="new-income" data-toggle="modal" data-target="#new-money-flow">New Income</a>
                    <hr class="primary">
                    <table class="table"  style="color: black;" id="table-incomes">
                        <thead>
                            <th>#</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Desciption</th>
                            <th>Amount</th>
                            <th></th>
                        </thead>
                        <tbody >
                        <?php
                            while ($row = mysql_fetch_assoc($incomes)) {  ?>
                            <tr data-id="<?php echo $row["id"]; ?>">
                                <td><?php echo $row["id"]; ?></td>
                                <td><?php echo $row["title"]; ?></td>
                                <td><?php echo $row["date"]; ?></td>
                                <td><?php echo $row["description"]; ?></td>
                                <td><?php echo $row["amount"]; ?></td>
                                <td class="text-right">
                                    <button class="edit-money-flow btn btn-default btn-xs" data-toggle="modal" data-target="#edit-money-flow">Edit</button>
                                    <button class="btn btn-default btn-xs del">Delete</button>
                                </td>
                            </tr>
                            <?php 
                            } 
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <?php include("includes/modal_form.php"); ?>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.js"></script>

    <script src="js/creative.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
