$(document).ready(function(){
    var ctx = document.getElementById('myChart');
    var myPieChart;

    var currentMonth = new Date().getMonth() + 1;

    $("option[value='"+currentMonth+"']").attr("selected","selected");
    createChart();

    $(document).on('click','#new-expence',function(){
        $('#new-money-flow').find('.modal-title').text('New Expence');
        $('#new-money-flow form #type').val('expence');
    });

    $(document).on('click','#new-income',function(){
        $('#new-money-flow').find('.modal-title').text('New Income');
        $('#new-money-flow form #type').val('income');
    });

    var edit_income = $('.edit-money-flow').parents('table').filter('[id="table-incomes"]');
    $(document).on('click',edit_income,function(){
        $('#edit-money-flow').find('.modal-title').text('Edit Income');
    });

    var edit_income = $('.edit-money-flow').parents('table').filter('[id="table-expences"]');
    $(document).on('click',edit_income,function(){
        $('#edit-money-flow').find('.modal-title').text('Edit Expences');
    });

    $(document).on('click', '#add-money-flow', function(e){

        e.preventDefault();
        $('#new-money-flow').modal('hide');

        var title = $('#new-money-flow form #title').val();
        var date = $('#new-money-flow form #date').val();
        var description = $('#new-money-flow form #description').val();
        var amount = $('#new-money-flow form #amount').val();
        var type = $('#new-money-flow form #type').val();

        $.ajax({
            url: 'php/add.php',
            method: 'POST', 
            data: {'title': title,'date': date,'description': description,'amount': amount,'type': type},
            dataType:'json',
            contentType: 'application/x-www-form-urlencoded',
            success: function(data) {
                var string =
                        '<tr data-id="'+data.id+'">'+
                            '<td>'+data.id+'</td>'+
                            '<td>'+title+'</td>'+
                            '<td>'+date+'</td>'+
                            '<td>'+description+'</td>'+
                            '<td>'+amount+'</td>'+
                            '<td class="text-right">'+
                                '<button class="edit-money-flow btn btn-default btn-xs" data-toggle="modal" data-target="#edit-income">Edit</button>'+
                                ' <button class="btn btn-default btn-xs del">Delete</button>'+
                            '</td>'+
                        '</tr>';

                $('#table-'+type+'s tbody').append(string);
                updateChart($('select').val());
            },
            error: function(e,status){
                console.log('Error Hello '+e.error);
            }
        });
    });

    $(document).on('click', '.edit-money-flow', function(e){
       
        var id = $(this).parents('tr').attr('data-id');

        $.ajax({
            url: 'php/get.php?id=' + id, 
            dataType: 'json',
            success: function(data) {
                $('#edit-money-flow form #id').val(data.id);
                $('#edit-money-flow form #title').val(data.title);
                $('#edit-money-flow form #date').val(data.date);
                $('#edit-money-flow form #description').val(data.description);
                $('#edit-money-flow form #amount').val(data.amount);
            },
            error: function(e,status){
                console.log(id_incomes+'Error Hello '+e.error);
            }
        });
    });

     $(document).on('click', '#update-money-flow', function(e){

        e.preventDefault();
        $('#edit-money-flow').modal('hide');

        var id = $('#edit-money-flow form #id').val();
        var title = $('#edit-money-flow form #title').val();
        var date = $('#edit-money-flow form #date').val();
        var description = $('#edit-money-flow form #description').val();
        var amount = $('#edit-money-flow form #amount').val();

        $.ajax({
            url: 'php/edit.php',
            method: 'POST', 
            data: {'title': title,'date': date,'description': description,'amount': amount,'id': id},
            contentType: 'application/x-www-form-urlencoded',
            success: function() {

                var tr = $("tr[data-id='"+id+"']");
                tr.children().each(function( index, element ) {
                        switch(index){
                            case 1:
                                $(this).text(title);
                                break;
                            case 2:
                                $(this).text(date);
                                break;
                            case 3:
                                $(this).text(description);
                                break;
                            case 4:
                                $(this).text(amount);
                                break;
                        }
                    }
                );
                updateChart($('select').val());
            },
            error: function(e,status){
                console.log('Error Hello '+e.error);
            }
        });
    });

    $(document).on('click', '.del', function(){

        var sure = confirm('Are you sure?');

        if (!sure) {
            return false;
        } else {
            var parent = $(this).parent().parent();
            var id = parent.attr('data-id');

            $.ajax({
                url: 'php/delete.php?id='+id,
                method: 'GET',
                success: function() {
                    parent.remove();
                    updateChart($('select').val());
                },
                error: function(e,status){
                    console.log('Error Hello '+e.error);
                }
            });

            return true;
        }
    });

    $(document).on('change', '#month', function(e){

        e.preventDefault();

        var selectedMonth = $('#month option:selected').val();
        updateChart(selectedMonth);
    });


    function createChart () {
        $.ajax({
            url: 'php/get_balance.php?month='+currentMonth,
            method: 'GET', 
            dataType: 'json',
            success: function(data) {
                
                myPieChart = new Chart(ctx,{
                    type: 'pie',
                    data: {
                        labels: [
                            'incomes',
                            'expences'
                        ],
                        datasets: [
                            {
                                data: [data.incomes, data.expences],
                                backgroundColor: [
                                    '#F05F40',
                                    '#d62c08'
                                ],
                                hoverBackgroundColor: [
                                    '#F05F40',
                                    '#d62c08'
                                ]
                            }]
                    }
                });
            },
            error: function(e,status){
                console.log('Error Hello '+e.error);
            }
        });
    }

    function updateChart (month) {
        $.ajax({
            url: 'php/get_balance.php?month='+month,
            method: 'GET', 
            dataType: 'json',
            success: function(data) {

                myPieChart.data.datasets[0].data[0] = data.incomes;
                myPieChart.data.datasets[0].data[1] = data.expences;
                myPieChart.update();
            },
            error: function(e,status){
                console.log('Error Hello '+e.error);
            }
        });
    }

});